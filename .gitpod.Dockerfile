FROM gitpod/workspace-full

USER root

RUN apt-get update && \
	apt-get install --yes software-properties-common && \
	add-apt-repository --yes ppa:luke-jr/bitcoincore && \
    apt-get update && \
	apt-get install --yes bitcoind make nano-tiny net-tools

ADD . /tmp/bitcoin-testnet-box

RUN cat /tmp/bitcoin-testnet-box/.bashrc >> /etc/bash.bashrc

# Added the installation for jq
RUN apt-get install -y jq

USER gitpod

